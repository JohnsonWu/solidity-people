import React, { useState, useEffect } from "react";
import "./App.css";
import peopleABI from "./config/abi/people.json";
import Web3 from "web3";

declare let window: any;

function App() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const peopleAddress = "0xcAdf9e50eF8A8BfDdBb0ff1A0Bd45cd09975D50C";

  let web3;
  
  if (typeof window !== "undefined" && typeof window.web3 !== "undefined") {
    // We are in the browser and metamask is running.
    web3 = new Web3(window.web3.currentProvider);
    var address = window.web3.currentProvider.selectedAddress;
    console.log(address);
  } else {
    // We are on the server *OR* the user is not running metamask
    const provider = new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/dd5e8c6ebc7340f3a800df5a4c9aa832");
    web3 = new Web3(provider);
    //window.web3.currentProvider.enable();
  }
  
  const peopleContract = new web3.eth.Contract(peopleABI as any, peopleAddress);

  useEffect(() => {
    console.log(peopleContract.methods);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const count = async () => {
    const peopleCount = await peopleContract.methods.peopleCount().call();
    const people = await peopleContract.methods.people(1).call();
    console.log("count", peopleCount);
    console.log("people", people);
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    console.log(firstName, lastName);
    try{
      peopleContract.methods.addPerson(firstName, lastName).send({ from: address });
    } catch(err) {
      console.error(err);
    }
  };
  
  const getResult = () => {
    count();
  }

  return (
    <div className="App">
      <form onSubmit={onSubmit}>
        <label>
          First Name
          <input type="text" name="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
        </label>
        <label>
          Last Name
          <input type="text" name="lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} />
        </label>
        <input type="submit" value="提交" />
      </form>
      <button onClick={getResult}>result</button>
    </div>
  );
}

export default App;
